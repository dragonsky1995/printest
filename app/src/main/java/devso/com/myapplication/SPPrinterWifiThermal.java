package devso.com.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.BitSet;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Anhdt lop in hoa don qua wifi
 */
public class SPPrinterWifiThermal extends SPPrinterRemote {

    public static final String TAG = SPPrinterWifiThermal.class.getSimpleName();

    private Socket mSocket;

    private String mIp = "192.168.30.250";

    private int mPort = 9100;

    private final byte[] INITIALIZE_PRINTER = new byte[]{0x1B, 0x40};

    private final byte[] PRINT_AND_FEED_PAPER = new byte[]{0x0A};

    private final byte[] BELL = new byte[]{0x07};

    private final byte[] SELECT_BIT_IMAGE_MODE = new byte[]{(byte) 0x1B, (byte) 0x2A};
    private final byte[] SET_LINE_SPACING = new byte[]{0x1B, 0x33};
    private final byte[] ALIGN_LEFT = new byte[]{0x1b, 'a', 0x00};

    byte[] setLineSpacing30Dots = buildPOSCommand(SET_LINE_SPACING, (byte) 30);

    public SPPrinterWifiThermal() {
    }

    private boolean mAcceptTryConnect = false;

    private final Object mObject = new Object();

    public int mRequest = 0;

    @Override
    public boolean print(@NonNull Bitmap bitmap, int style, boolean printAgain,
                         ConnectHandler connectHandler, String applyFor) {

        // thu ket noi lai 2 lan
        int countTryConnect = 1;
        if (mAcceptTryConnect) {
            countTryConnect = 2;
        }
        synchronized (mObject) {
            while (countTryConnect > 0) {
                countTryConnect--;
                try {
                    Log.v("Anhdt", "print prepare");
                    if (mSocket == null || !mSocket.isConnected() || mSocket.isClosed()) {
                        mSocket = new Socket(Inet4Address.getByName(getIp()),
                                getPort());
                    }

                    if (!mSocket.isClosed() && mSocket.isConnected()) {
                        OutputStream outputStream = mSocket.getOutputStream();
                        try {
                            printerBitMap(bitmap, outputStream);
                            outputStream.write(cutPaper());
                        } catch (Exception e) {
                            mSocket.close();
                            Log.v("Anhdt", "catchExcept " + e.toString());
                        }

                        mRequest--;
                        outputStream.flush();

                        if (mRequest <= 0) {
                            outputStream.close();
                            mSocket.close();
                            mSocket = null;
                        }
                        Log.v("Anhdt", "print success " + mRequest);

                        cacheEnableConnect(true);
                        return true;
                    }
                } catch (Exception e) {
                    Log.v("Anhdt", "catchExcept " + e.toString());
                    if (countTryConnect == 0) {
                        mRequest--;
                        mSocket = null;
                        if (mAcceptTryConnect) {
                            // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                            cacheEnableConnect(false);
                            return false;
                        }
                        cacheEnableConnect(false);
                        return false;
                    }
                }
            }
            mSocket = null;
            mRequest--;
            if (mAcceptTryConnect) {
                // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                cacheEnableConnect(false);
                return false;
            }
            cacheEnableConnect(false);
            return true;
        }
    }

    public boolean print2(int style, boolean printAgain,
                          ConnectHandler connectHandler, String applyFor) {

        // thu ket noi lai 2 lan
        int countTryConnect = 1;
        if (mAcceptTryConnect) {
            countTryConnect = 2;
        }
        synchronized (mObject) {
            while (countTryConnect > 0) {
                countTryConnect--;
                try {
                    Log.v("Anhdt", "print prepare");
                    if (mSocket == null || !mSocket.isConnected() || mSocket.isClosed()) {
                        mSocket = new Socket(Inet4Address.getByName(getIp()),
                                getPort());
                    }

                    if (!mSocket.isClosed() && mSocket.isConnected()) {
                        OutputStream outputStream = mSocket.getOutputStream();
                        try {
                            printer(outputStream);
                            outputStream.write(cutPaper());
                        } catch (Exception e) {
                            mSocket.close();
                            Log.v("Anhdt", "catchExcept " + e.toString());
                        }

                        mRequest--;
                        outputStream.flush();

                        if (mRequest <= 0) {
                            outputStream.close();
                            mSocket.close();
                            mSocket = null;
                        }
                        Log.v("Anhdt", "print success " + mRequest);

                        cacheEnableConnect(true);
                        return true;
                    }
                } catch (Exception e) {
                    Log.v("Anhdt", "catchExcept " + e.toString());
                    if (countTryConnect == 0) {
                        mRequest--;
                        mSocket = null;
                        if (mAcceptTryConnect) {
                            // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                            cacheEnableConnect(false);
                            return false;
                        }
                        cacheEnableConnect(false);
                        return false;
                    }
                }
            }
            mSocket = null;
            mRequest--;
            if (mAcceptTryConnect) {
                // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                cacheEnableConnect(false);
                return false;
            }
            cacheEnableConnect(false);
            return true;
        }
    }

    public boolean printString(String msg, int function) {
        return printString(msg, 0, false);
    }

    public boolean printString(String msg, int function, boolean bell) {
        // thu ket noi lai 2 lan
        int countTryConnect = 1;
        if (mAcceptTryConnect) {
            countTryConnect = 2;
        }
        synchronized (mObject) {
            while (countTryConnect > 0) {
                countTryConnect--;
                try {
                    Log.v("Anhdt", "print prepare");
                    if (mSocket == null || !mSocket.isConnected() || mSocket.isClosed()) {
                        mSocket = new Socket(Inet4Address.getByName(getIp()),
                                getPort());
                    }

                    if (!mSocket.isClosed() && mSocket.isConnected()) {
                        BufferedOutputStream outputStream = new BufferedOutputStream(mSocket.getOutputStream());
                        try {
                            outputStream.write(INITIALIZE_PRINTER);

                            if (bell) {
                                outputStream.write(BELL);
                                outputStream.write(PRINT_AND_FEED_PAPER);
                            }

                            if (function == 48) {
                                outputStream.write(beepCmd48());
                                Log.d("Anhdt", "Fn48");
                            } else if (function == 9723) {
                                outputStream.write(beepCmd97_TMU230());
                                Log.d("Anhdt", "Fn97 230");
                            } else if (function == 9788) {
                                outputStream.write(beepCmd97_TMU88V());
                                Log.d("Anhdt", "Fn97 88V");
                            }
                            outputStream.write(PRINT_AND_FEED_PAPER);

                            outputStream.write(msg.getBytes());

                            outputStream.write(PRINT_AND_FEED_PAPER);
                            outputStream.write(setLineSpacing30Dots);

                            outputStream.write(cutPaper());
                            outputStream.flush();

                            Log.d("Anhdt", "Gui thanh cong");
                        } catch (Exception e) {
                            mSocket.close();
                            Log.v("Anhdt", "catchExcept " + e.toString());
                            mSocket = null;
                        }

                        if (mRequest - 1 <= 0) {
                            outputStream.close();
                            if (mSocket != null) {
                                mSocket.close();
                            }
                        }
                        Log.v("Anhdt", "print success " + mRequest);

                        cacheEnableConnect(true);
                        mRequest--;
                        return true;
                    }
                } catch (Exception e) {
                    Log.v("Anhdt", "catchExcept " + e.toString());
                    if (countTryConnect == 0) {
                        mRequest--;
                        mSocket = null;
                        if (mAcceptTryConnect) {
                            // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                            cacheEnableConnect(false);
                            return false;
                        }
                        cacheEnableConnect(false);
                        return false;
                    }
                }
            }
            mSocket = null;
            mRequest--;

            if (mAcceptTryConnect) {
                // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                cacheEnableConnect(false);
                return false;
            }
            cacheEnableConnect(false);
            return true;
        }
    }

    public boolean printStringRS(String msg) {
        // thu ket noi lai 2 lan
        int countTryConnect = 1;
        if (mAcceptTryConnect) {
            countTryConnect = 2;
        }
        synchronized (mObject) {
            while (countTryConnect > 0) {
                countTryConnect--;
                try {
                    Log.v("Anhdt", "print prepare");
                    if (mSocket == null || !mSocket.isConnected() || mSocket.isClosed()) {
                        mSocket = new Socket(Inet4Address.getByName(getIp()),
                                getPort());
                    }

                    if (!mSocket.isClosed() && mSocket.isConnected()) {
                        BufferedOutputStream outputStream = new BufferedOutputStream(mSocket.getOutputStream());
                        try {
                            outputStream.write(INITIALIZE_PRINTER);
                            outputStream.write(beepCmdRS());
                            outputStream.write(PRINT_AND_FEED_PAPER);

                            outputStream.write(msg.getBytes());

                            outputStream.write(PRINT_AND_FEED_PAPER);
                            outputStream.write(setLineSpacing30Dots);

                            outputStream.write(cutPaper());
                            outputStream.flush();
                        } catch (Exception e) {
                            mSocket.close();
                            Log.v("Anhdt", "catchExcept " + e.toString());
                            mSocket = null;
                        }

                        if (mRequest - 1 <= 0) {
                            outputStream.close();
                            if (mSocket != null) {
                                mSocket.close();
                            }
                        }
                        Log.v("Anhdt", "print success " + mRequest);

                        cacheEnableConnect(true);
                        mRequest--;
                        return true;
                    }
                } catch (Exception e) {
                    Log.v("Anhdt", "catchExcept " + e.toString());
                    if (countTryConnect == 0) {
                        mRequest--;
                        mSocket = null;
                        if (mAcceptTryConnect) {
                            // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                            cacheEnableConnect(false);
                            return false;
                        }
                        cacheEnableConnect(false);
                        return false;
                    }
                }
            }
            mSocket = null;
            mRequest--;
            if (mAcceptTryConnect) {
                // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                cacheEnableConnect(false);
                return false;
            }
            cacheEnableConnect(false);
            return true;
        }
    }

    public boolean sendBeepCmd() {
        // thu ket noi lai 2 lan
        int countTryConnect = 1;
        if (mAcceptTryConnect) {
            countTryConnect = 2;
        }
        synchronized (mObject) {
            while (countTryConnect > 0) {
                countTryConnect--;
                try {
                    Log.v("Anhdt", "print prepare");
                    if (mSocket == null || !mSocket.isConnected() || mSocket.isClosed()) {
                        mSocket = new Socket(Inet4Address.getByName(getIp()),
                                getPort());
                    }

                    if (!mSocket.isClosed() && mSocket.isConnected()) {
                        OutputStream outputStream = mSocket.getOutputStream();
                        try {
                            outputStream.write(INITIALIZE_PRINTER);
                            outputStream.write(BELL);
                            outputStream.write(BELL);
                            outputStream.write(BELL);

                            outputStream.write(PRINT_AND_FEED_PAPER);
                            outputStream.write(setLineSpacing30Dots);

                            outputStream.write(cutPaper());
                        } catch (Exception e) {
                            mSocket.close();
                            Log.v("Anhdt", "catchExcept " + e.toString());
                            mSocket = null;
                        }

                        if (mRequest - 1 <= 0) {
                            outputStream.close();
                            if (mSocket != null) {
                                mSocket.close();
                            }
                        }
                        Log.v("Anhdt", "print success " + mRequest);

                        cacheEnableConnect(true);
                        mRequest--;
                        return true;
                    }
                } catch (Exception e) {
                    Log.v("Anhdt", "catchExcept " + e.toString());
                    if (countTryConnect == 0) {
                        mRequest--;
                        mSocket = null;
                        if (mAcceptTryConnect) {
                            // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                            cacheEnableConnect(false);
                            return false;
                        }
                        cacheEnableConnect(false);
                        return false;
                    }
                }
            }
            mSocket = null;
            mRequest--;
            if (mAcceptTryConnect) {
                // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                cacheEnableConnect(false);
                return false;
            }
            cacheEnableConnect(false);
            return true;
        }
    }

    public boolean drawerKickout(int pin) {
        // thu ket noi lai 2 lan
        int countTryConnect = 1;
        if (mAcceptTryConnect) {
            countTryConnect = 2;
        }
        synchronized (mObject) {
            while (countTryConnect > 0) {
                countTryConnect--;
                try {
                    Log.v("Anhdt", "print prepare");
                    if (mSocket == null || !mSocket.isConnected() || mSocket.isClosed()) {
                        mSocket = new Socket(Inet4Address.getByName(getIp()),
                                getPort());
                    }

                    if (!mSocket.isClosed() && mSocket.isConnected()) {
                        BufferedOutputStream outputStream = new BufferedOutputStream(mSocket.getOutputStream());
                        try {
                            outputStream.write(INITIALIZE_PRINTER);

                            if (pin ==2 ) {
                                outputStream.write(drawerKickoutPin2());
                                Log.d("Anhdt", "drawerKickout Pin2");
                            } else if (pin == 5) {
                                outputStream.write(drawerKickoutPin5());
                                Log.d("Anhdt", "drawerKickout Pin5");
                            }

                            outputStream.write(PRINT_AND_FEED_PAPER);
                            outputStream.write(setLineSpacing30Dots);

                            outputStream.write(cutPaper());
                            outputStream.flush();

                            Log.d("Anhdt", "Gui thanh cong");
                        } catch (Exception e) {
                            mSocket.close();
                            Log.v("Anhdt", "catchExcept " + e.toString());
                            mSocket = null;
                        }

                        if (mRequest - 1 <= 0) {
                            outputStream.close();
                            if (mSocket != null) {
                                mSocket.close();
                            }
                        }
                        Log.v("Anhdt", "print success " + mRequest);

                        cacheEnableConnect(true);
                        mRequest--;
                        return true;
                    }
                } catch (Exception e) {
                    Log.v("Anhdt", "catchExcept " + e.toString());
                    if (countTryConnect == 0) {
                        mRequest--;
                        mSocket = null;
                        if (mAcceptTryConnect) {
                            // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                            cacheEnableConnect(false);
                            return false;
                        }
                        cacheEnableConnect(false);
                        return false;
                    }
                }
            }
            mSocket = null;
            mRequest--;

            if (mAcceptTryConnect) {
                // chi thong bao khi mat ket noi lan dau, nhung lan sau thu lai thi khong thong bao
                cacheEnableConnect(false);
                return false;
            }
            cacheEnableConnect(false);
            return true;
        }
    }
    // ESC  (   A   pL  pH  fn  n   c   t
    private byte[] beepCmd48() {
//        return new byte[]{0x1B, 0x28, 0x41, 0x04, 0x00, 0x30, 0x37, 0x03, 0x0A};
        return new byte[]{0x1B, 0x28, 0x41, 0x04, 0x00, 0x30, 0x32, 0x03, 0x14};
    }

    // ESC  (   A   pL  pH  fn  n   c   t1  t2
    private byte[] beepCmd97_TMU230() {
//        return new byte[]{0x1B, 0x28, 0x41, 0x04, 0x00, 0x61, 0x37, 0x03, 0x0A};
        return new byte[]{0x1B, 0x28, 0x41, 0x05, 0x00, 0x61, 0x64, 0x03, 0x05, 0x05};
    }

    // ESC  (   A   pL  pH  fn  n   c
    private byte[] beepCmd97_TMU88V() {
        return new byte[]{0x1B, 0x28, 0x41, 0x03, 0x00, 0x61, 0x01, 0x03};
    }

    // ESC  p   m   t1  t2
    private byte[] drawerKickoutPin2() {
        return new byte[]{0x1B, 0x70, 0x00, 0x64, 0x64};
    }

    // ESC  p   m   t1  t2
    private byte[] drawerKickoutPin5() {
        return new byte[]{0x1B, 0x70, 0x01, 0x64, 0x64};
    }

    private byte[] beepCmdRS() {
        return new byte[]{0x1E};
    }

    private void printer(OutputStream outputStream) {
        // TODO test in o day
        byte[] align_left = new byte[]{0x1B, 0x61, 0x00};
        byte[] align_center = new byte[]{0x1B, 0x61, 0x01};
        byte[] align_right = new byte[]{0x1B, 0x61, 0x02};

//        byte[] select_VN_char_code_table = new byte[]{0x1B, 0x74, 0x1B};

        String message = "Hello World\n";

        try {
//            outputStream.write(INITIALIZE_PRINTER);
//            outputStream.write("123456789.123456789.123456789.123456789.123456789.123456789.123456789.123456789.".getBytes());
//            newLine(outputStream);
//            newLine(outputStream);
//            printTo3Columns(outputStream, "Item", "Quantity", "Price");
//            newLine(outputStream);
//
//            printTo3Columns(outputStream, "Coca", "10", "8000");
//            printTo3Columns(outputStream, "Pepsi", "5", "11000");
//            printTo3Columns(outputStream, "Sting", "5", "10000");
//            printTo3Columns(outputStream, "Một hai ba bốn", "135", "123456789");
//
//            newLine(outputStream);
//            newLine(outputStream);
//
//            printAlign(outputStream, "Align Left", Align.ALIGN_LEFT);
//            newLine(outputStream);
//
//            printAlign(outputStream, "Align Center", Align.ALIGN_CENTER);
//            newLine(outputStream);
//
//            printAlign(outputStream, "Align Right", Align.ALIGN_RIGHT);
//            newLine(outputStream);
//
//            outputStream.write(INITIALIZE_PRINTER);
//
//            printWithChracterSize(outputStream,"Normal size", CharacterSize.NORMAL);
//            newLine(outputStream);
//
//            printWithChracterSize(outputStream,"Double Width", CharacterSize.DOUBLE_WIDTH);
//            newLine(outputStream);
//
//            outputStream.write(INITIALIZE_PRINTER);
//            printWithChracterSize(outputStream,"Double Height", CharacterSize.DOUBLE_HEIGHT);
//            newLine(outputStream);
//
//            printEmphasized(outputStream, "Emphasized");
//            newLine(outputStream);
//
//            printWithCharacterFont(outputStream, "Font 9x17", CharacterFont.Font_9_17);
//            newLine(outputStream);
//
//            printWithCharacterFont(outputStream, "Font 12x24", CharacterFont.Font_12_24);
//            newLine(outputStream);
//
//            printDoubleStrike(outputStream, "Double Strike");
//            newLine(outputStream);
//
//            printUnderlined(outputStream, "Underlined text: 1-dot-thick", UnderLinedMode.ONE_DOT_THICK);
//            newLine(outputStream);
//
//            printUnderlined(outputStream, "Underlined text: 2-dot-thick", UnderLinedMode.TWO_DOT_THICK);


//            for (int i=0; i<=89; i++) {
//                byte [] select_VN =  new byte[]{0x1B, 0x74};
//                byte[] select_VN_char_code_table = buildPOSCommand(select_VN, (byte)i);
//                outputStream.write(select_VN_char_code_table);
//                Charset myCharset = Charset.forName("TCVN-3-1");
//                outputStream.write((i+" òa òe\t").getBytes(myCharset));
//
//            }

//            byte[] select_VN_char_code_table = new byte[]{0x1B, 0x74, 0x34};
//            outputStream.write(select_VN_char_code_table);
//            byte[] may_in = strTobytes("Tiếng việt có dấu");
//            outputStream.write(may_in);

//            for (int i = 0; i < 95; i++) {
            byte[] select_VN_char_code_table = new byte[]{0x1B, 0x74, (byte) 52};
            outputStream.write(select_VN_char_code_table);

//                outputStream.write(("UTF-8: Tiếng Việt È Ầ a Á ú Ê Ế\n").getBytes(Charset.forName("cp1258")));
            Log.v("Anhdt", "byte " + Arrays.toString("Ê Ê Ầ a Á ú Ê Ế\n".getBytes(Charset.forName("UTF-8"))));
//                outputStream.write("Ê Ê Ầ a Á ú Ê Ế\n".getBytes(Charset.forName("UTF-8")));

//            byte[] array = new byte[512];
//            for (int j = -256; j < 0; j++) {
//                array[j + 256] = (byte) j;
//            }
//            outputStream.write(array);
//            for (int j = -512; j < 0; j++) {
//                if (j % 2 == 0) {
//                    array[j + 512] = (byte) -191;
//                } else {
//                    array[j + 512] = (byte) (j / 2);
//                }
//            }
//            outputStream.write(array);
//
//            outputStream.write("máy in bếp".getBytes("UTF-8"));
//            }

//                outputStream.write("ISO-8859-1: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.ISO_8859_1));
//                outputStream.write("US-ASCII: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.US_ASCII));
//                outputStream.write("UTF-16BE: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.UTF_16LE));
//                outputStream.write("UTF-16BE: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.UTF_16));
//
//            select_VN_char_code_table = new byte[]{0x1B, 0x74, 0x1B/*(byte) i*/};
//            outputStream.write(select_VN_char_code_table);
//
//            outputStream.write("UTF-8: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.UTF_8));
//            outputStream.write("ISO-8859-1: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.ISO_8859_1));
//            outputStream.write("US-ASCII: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.US_ASCII));
//            outputStream.write("UTF-16BE: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.UTF_16LE));
//            outputStream.write("UTF-16BE: Tiếng Việt a ă â ắ ấ\n".getBytes(StandardCharsets.UTF_16));

//            }
            Log.v("Phungtd", "á to byte "+ Arrays.toString("á".getBytes(Charset.forName("UTF-8")))); // [-61, -95]
            outputStream.write('á');
            outputStream.write(-61);
            outputStream.write("á".getBytes());
            outputStream.write(new byte[]{-61, -95});
            newLine(outputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] strTobytes(String str, String charset) {
        byte[] b = null, data = null;
        try {
            b = str.getBytes("utf-8");
            data = new String(b, "utf-8").getBytes("gbk");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return data;
    }

    public static byte[] strTobytes(String str) {
        byte[] b = null, data = null;
        data = str.getBytes(StandardCharsets.UTF_8);
        return data;
    }

    private void newLine(OutputStream outputStream) throws IOException {
//        outputStream.write("\n".getBytes());
        outputStream.write(0x0A);
    }

    // Align Left -> print 1st String -> tab -> print 2nd String -> tab -> print 3rd String
    private void printTo3Columns(OutputStream outputStream, String message1, String message2, String message3) throws IOException {
        cancelPreviousTabSetting(outputStream);
        byte[] set_horizontal_tab_position = new byte[]{0x1B, 0x44, 0x1C, 0x26, 0x00}; // Set tab stops at 28 and 38 characters (total: 48 characters = 28 + 10 + 10)
        outputStream.write(set_horizontal_tab_position);

        byte[] align_left = new byte[]{0x1B, 0x61, 0x00};
        outputStream.write(align_left);

        outputStream.write(message1.getBytes());
        outputStream.write(0x09);   // tab

        outputStream.write(message2.getBytes());
        outputStream.write(0x09);   // tab

//        byte[] align_right = new byte[]{0x1B, 0x61, 0x02};
//        outputStream.write(align_right);
//        outputStream.write(message3.getBytes());
        outputStream.write(create_n_character_String(message3, 10).getBytes());

        newLine(outputStream);
//        outputStream.write(align_left);
    }

    private String create_n_character_String(String string, int numOfChracters) {
        if (string.length() >= numOfChracters) return string;
        StringBuilder stringBuilder = new StringBuilder(string);
        for (int i = 0; i < (numOfChracters - string.length()); i++) {
            stringBuilder.insert(0, " ");
        }
        return stringBuilder.toString();
    }


    private void cancelPreviousTabSetting(OutputStream outputStream) throws IOException {
        byte[] cancel_setting = new byte[]{0x1B, 0x44, 0x00};
        outputStream.write(cancel_setting);
    }

    private void printEmphasized(OutputStream outputStream, String message) throws IOException {
        byte[] emphasized_on = new byte[]{0x1B, 0x45, 0x01};
        byte[] emphasized_off = new byte[]{0x1B, 0x45, 0x00};
        outputStream.write(emphasized_on);
        outputStream.write(message.getBytes());
        outputStream.write(emphasized_off);
    }

    private void printDoubleStrike(OutputStream outputStream, String message) throws IOException {
        byte[] double_strike_on = new byte[]{0x1B, 0x47, 0x01};
        byte[] double_strike_off = new byte[]{0x1B, 0x47, 0x00};
        outputStream.write(double_strike_on);
        outputStream.write(message.getBytes());
        outputStream.write(double_strike_off);
    }

    private void printUnderlined(OutputStream outputStream, String message, UnderLinedMode mode) throws IOException {
        byte[] underlined_1_dot = new byte[]{0x1B, 0x2D, 0x01};
        byte[] underlined_2_dot = new byte[]{0x1B, 0x2D, 0x02};
        byte[] underlined_off = new byte[]{0x1B, 0x2D, 0x00};
        if (mode == UnderLinedMode.ONE_DOT_THICK) {
            outputStream.write(underlined_1_dot);
        } else if (mode == UnderLinedMode.TWO_DOT_THICK) {
            outputStream.write(underlined_2_dot);
        }
        outputStream.write(message.getBytes());
        outputStream.write(underlined_off);
    }

    private void printWithCharacterFont(OutputStream outputStream, String message, CharacterFont font) throws IOException {
        byte[] font_12_24 = new byte[]{0x1B, 0x4D, 0x00};
        byte[] font_9_17 = new byte[]{0x1B, 0x4D, 0x01};

        if (font == CharacterFont.Font_12_24) {
            outputStream.write(font_12_24);
        } else if (font == CharacterFont.Font_9_17) {
            outputStream.write(font_9_17);
        }
        outputStream.write(message.getBytes());
    }

    private void printWithChracterSize(OutputStream outputStream, String message, CharacterSize size) throws IOException {
        byte[] normal_size = new byte[]{0x1D, 0x21, 0x00};
        byte[] double_width = new byte[]{0x1D, 0x21, 0x10};
        byte[] double_height = new byte[]{0x1D, 0x21, 0x11};

        switch (size) {
            case NORMAL: {
                outputStream.write(normal_size);
                outputStream.write(message.getBytes());
                break;
            }
            case DOUBLE_WIDTH: {
                outputStream.write(double_width);
                outputStream.write(message.getBytes());
                outputStream.write(normal_size);
                break;
            }
            case DOUBLE_HEIGHT: {
                outputStream.write(double_height);
                outputStream.write(message.getBytes());
                outputStream.write(normal_size);
                break;
            }
        }
    }

    private void printAlign(OutputStream outputStream, String message, Align alignType) throws IOException {
        switch (alignType) {
            case ALIGN_LEFT: {
                printAlignLeft(outputStream, message);
                break;
            }
            case ALIGN_CENTER: {
                printAlignCenter(outputStream, message);
                break;
            }
            case ALIGN_RIGHT: {
                printAlignRight(outputStream, message);
                break;
            }
        }
    }

    private void printAlignLeft(OutputStream outputStream, String message) throws IOException {
        byte[] align_left = new byte[]{0x1B, 0x61, 0x00};
        outputStream.write(align_left);
        outputStream.write(message.getBytes());
    }

    private void printAlignCenter(OutputStream outputStream, String message) throws IOException {
        byte[] align_center = new byte[]{0x1B, 0x61, 0x01};
        outputStream.write(align_center);
        outputStream.write(message.getBytes());
    }

    private void printAlignRight(OutputStream outputStream, String message) throws IOException {
        byte[] align_right = new byte[]{0x1B, 0x61, 0x02};
        outputStream.write(align_right);
        outputStream.write(message.getBytes());
    }

    @NotNull
    @Override
    public String getIp() {
        return mIp;
    }

    @Override
    public int getPort() {
        return mPort;
    }

    @Override
    public void setPort(int port) {
        mPort = port;
    }

    @Override
    public void setIp(@NotNull String ip) {
        mIp = ip;
    }

    public interface ConnectHandler {

        void handlerFalse(SPPrinterRemote remote, Bitmap bitmap, String type);

    }

    @NonNull
    @Override
    public Single<Boolean> isConnect() {
        Log.v("Anhdt", "isConnectWifi");
        return Single.create(emitter -> {
            Log.v("Anhdt", "isConnectWifi run " + getIp() + " " + mSocket);
            if (TextUtils.isEmpty(getIp())) {
                emitter.onSuccess(false);
                emitter.isDisposed();
            } else {
                try {
                    Log.v("Anhdt", "isConnectWifi close");
                    mSocket = new Socket(Inet4Address.getByName(getIp()), getPort());
                    Log.v("Anhdt", "isConnectWifi connect");

                    if (mSocket.isConnected() || !mSocket.isClosed()) {
                        Log.v("Anhdt", "isConnectWifi true");
                        cacheEnableConnect(true);
                        emitter.onSuccess(true);
                    } else {
                        mSocket.close();
                        cacheEnableConnect(false);
                        Log.v("Anhdt", "isConnectWifi false");
                        emitter.onSuccess(false);
                    }
                    mSocket.close();
                    mSocket = null;
                } catch (Exception io) {
                    io.printStackTrace();
                    cacheEnableConnect(false);
                    Log.v("Anhdt", "isConnectWifi false " + io);
                    if (mSocket != null) {
                        mSocket.close();
                        mSocket = null;
                    }
                    emitter.onSuccess(false);
                }
            }
        });
    }

    protected boolean mLastIsConnect = false;

    private void cacheEnableConnect(Boolean isEnable) {
        mLastIsConnect = isEnable;
        mAcceptTryConnect = isEnable;
    }

    @Override
    public boolean getLastConnect(boolean isCheckAgain, String applyFor) {
        if (isCheckAgain) {
            isConnect().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }
        return mLastIsConnect;
    }

    @SuppressLint("CheckResult")
    public void setWifiIPAddress(Context context, String ipAddress, int port) {
        this.setIp(ipAddress);
        this.setPort(port);
    }

    /**
     *
     */
    public void printerBitMap(Bitmap bitmap, OutputStream printOutput) throws Exception {
        BitSet imageBits = getBitsImageData(bitmap);
        byte widthLSB = (byte) (bitmap.getWidth() & 0xFF);
        byte widthMSB = (byte) ((bitmap.getWidth() >> 8) & 0xFF);

        // COMMANDS
        byte[] selectBitImageModeCommand = buildPOSCommand(SELECT_BIT_IMAGE_MODE, (byte) 33,
                widthLSB, widthMSB);
        byte[] setLineSpacing24Dots = buildPOSCommand(SET_LINE_SPACING, (byte) 24);
        byte[] setLineSpacing30Dots = buildPOSCommand(SET_LINE_SPACING, (byte) 30);

        printOutput.write(INITIALIZE_PRINTER);
        printOutput.write(setLineSpacing24Dots);
//        printOutput.write(ALIGN_CENTER);
//        printOutput.write(makeImageToBytesArray(printerModel.getPath(), height, width));
        printOutput.write(ALIGN_LEFT);

        int offset = 0;
        while (offset < bitmap.getHeight()) {
            printOutput.write(selectBitImageModeCommand);

            int imageDataLineIndex = 0;
            byte[] imageDataLine = new byte[3 * bitmap.getWidth()];

            for (int x = 0; x < bitmap.getWidth(); ++x) {

                // Remember, 24 dots = 24 bits = 3 bytes.
                // The 'k' variable keeps track of which of those
                // three bytes that we're currently scribbling into.
                for (int k = 0; k < 3; ++k) {
                    byte slice = 0;

                    // A byte is 8 bits. The 'b' variable keeps track
                    // of which bit in the byte we're recording.
                    for (int b = 0; b < 8; ++b) {
                        // Calculate the y position that we're currently
                        // trying to draw. We take our offset, divide it
                        // by 8 so we're talking about the y offset in
                        // terms of bytes, add our current 'k' byte
                        // offset to that, multiple by 8 to get it in terms
                        // of bits again, and add our bit offset to it.
                        int y = (((offset / 8) + k) * 8) + b;

                        // Calculate the location of the pixel we want in the bit array.
                        // It'll be at (y * width) + x.
                        int i = (y * bitmap.getWidth()) + x;

                        // If the image (or this stripe of the image)
                        // is shorter than 24 dots, pad with zero.
                        boolean v = false;
                        if (i < imageBits.length()) {
                            v = imageBits.get(i);
                        }
                        // Finally, store our bit in the byte that we're currently
                        // scribbling to. Our current 'b' is actually the exact
                        // opposite of where we want it to be in the byte, so
                        // subtract it from 7, shift our bit into place in a temp
                        // byte, and OR it with the target byte to get it into there.
                        slice |= (byte) ((v ? 1 : 0) << (7 - b));
                    }

                    imageDataLine[imageDataLineIndex + k] = slice;

                    // Phew! Write the damn byte to the buffer
                }

                imageDataLineIndex += 3;
            }

            printOutput.write(imageDataLine);
            offset += 24;
            printOutput.write(PRINT_AND_FEED_PAPER);
        }

        printOutput.write(setLineSpacing30Dots);
    }

    private byte[] buildPOSCommand(byte[] command, byte... args) {
        byte[] posCommand = new byte[command.length + args.length];

        System.arraycopy(command, 0, posCommand, 0, command.length);
        System.arraycopy(args, 0, posCommand, command.length, args.length);

        return posCommand;
    }

    private BitSet getBitsImageData(Bitmap image) {
        int threshold = 127;
        int index = 0;
        int dimenssions = image.getWidth() * image.getHeight();
        BitSet imageBitsData = new BitSet(dimenssions);

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int color = image.getPixel(x, y);
                int red = (color & 0x00ff0000) >> 16;
                int green = (color & 0x0000ff00) >> 8;
                int blue = color & 0x000000ff;
                int luminance = (int) (red * 0.3 + green * 0.59 + blue * 0.11);
                //dots[index] = (luminance < threshold);
                imageBitsData.set(index, (luminance < threshold));
                index++;
            }
        }

        return imageBitsData;
    }

    private byte[] cutPaper() {
        return new byte[]{0x1D, 0x56, 0x41, 0x10};
    }

}

enum Align {
    ALIGN_LEFT,
    ALIGN_CENTER,
    ALIGN_RIGHT
}

enum CharacterSize {
    NORMAL,
    DOUBLE_HEIGHT,
    DOUBLE_WIDTH
}

enum CharacterFont {
    Font_12_24, // 12x24
    Font_9_17  // 9x17
}

enum UnderLinedMode {
    ONE_DOT_THICK,
    TWO_DOT_THICK
}