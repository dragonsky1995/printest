package devso.com.myapplication

import android.graphics.Bitmap
import io.reactivex.Single

/**
 * Created by Anhdt on 10/07/2019.
 */
public abstract class SPPrinterRemote {

    abstract fun getLastConnect(isCheckAgain: Boolean = false, applyFor: String = ""): Boolean

    abstract fun getIp(): String

    abstract fun getPort(): Int

    abstract fun setPort(port: Int)

    abstract fun setIp(ip: String)

    abstract fun print(bitmap: Bitmap, style: Int, printAgain: Boolean = false, connectFalse: SPPrinterWifiThermal.ConnectHandler? = null, applyFor: String = ""): Boolean

    abstract fun isConnect(): Single<Boolean>

}