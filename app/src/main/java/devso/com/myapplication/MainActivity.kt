package devso.com.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import java.util.concurrent.Executors
import android.graphics.Bitmap
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Typeface
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.marginLeft
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
//    private lateinit var etIP : EditText
//    private lateinit var etPort : EditText
//    private lateinit var btnPrint : Button
//    private lateinit var btnBeep48 : Button
//    private lateinit var btnBeep97 : Button
//    private lateinit var btnBeepRS : Button
//    private lateinit var ivImage : ImageView
    private val mPrinterRemote : SPPrinterWifiThermal = SPPrinterWifiThermal()
    private val executorService = Executors.newSingleThreadExecutor()

    private lateinit var itemList: MutableList<Item>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        findById()

        btnPrint.setOnClickListener {
//            createDumpData()
//            val image = createBitmapFromView(itemList, this)
//            ivImage.setImageBitmap(image)

            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
//                mPrinterRemote.print2(0,false,null,"")
                mPrinterRemote.printString("Hello World", 0)
            }
        }

        btnBeep48.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.printString("Hello World48", 48)
            }
        }

        btnBeep9723.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.printString("Hello World97 23", 9723)
            }
        }

        btnBeep9788.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.printString("Hello World97 88", 9788)
            }
        }

        btnBeepRS.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.printStringRS("Hello worldRS...")
            }
        }

        btnDrawer2.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.drawerKickout(2)
            }
        }

        btnDrawer5.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.drawerKickout(5)
            }
        }

        btnBell.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.printString("Hello World97 88", 0, true)
            }
        }

        btnBell2.setOnClickListener {
            mPrinterRemote.setIp(etIP.text.toString())
            mPrinterRemote.setPort(etPort.text.toString().toInt())

            executorService.execute {
                mPrinterRemote.sendBeepCmd()
            }
        }

    }

//    private fun findById() {
//        etIP = findViewById(R.id.etIP)
//        etPort = findViewById(R.id.etPort)
//        btnPrint = findViewById(R.id.btnPrint)
//        btnBeep48 = findViewById(R.id.btnBeep48)
//        btnBeep97 = findViewById(R.id.btnBeep97)
//        btnBeepRS = findViewById(R.id.btnBeepRS)
//        ivImage = findViewById(R.id.ivImage)
//    }

    private fun createDumpData() {
        itemList = ArrayList()
        itemList.add(Item("Coca", 5, 8000))
        itemList.add(Item("Pepsi", 13, 9000))
        itemList.add(Item("Redbull", 5, 11000))
        itemList.add(Item("Một tên rất dài", 20, 8500))
        itemList.add(Item("Một tên siêu siêu dài nữa", 5, 15500))
//        itemList.add(Item("Thêm một tên siêu siêu siêu siêu siêu siêu dài nữa", 5, 15500))
    }
/*
    private fun createBitmap() : Bitmap {
        val w = 100
        val h = 100
        val conf = Bitmap.Config.ARGB_8888 // see other conf types
        val bmp = Bitmap.createBitmap(w, h, conf)
        val canvas = Canvas(bmp)
        canvas.drawColor(Color.BLACK)
        return bmp
    }

    private fun createBitmapFromView(items: List<Item>, context: Context) : Bitmap {
        val rootView = LinearLayout(context)
        rootView.layoutParams = LinearLayout.LayoutParams(500,LinearLayout.LayoutParams.WRAP_CONTENT)
        rootView.orientation = LinearLayout.VERTICAL
        rootView.setBackgroundColor(Color.WHITE)

        for (item in items) {
            val itemView = createViewChild(item, context)
            rootView.addView(itemView)
        }

        rootView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))

        rootView.layout(0, 0, rootView.measuredWidth, rootView.measuredHeight)

        val bitmap = Bitmap.createBitmap(rootView.measuredWidth, rootView.measuredHeight, Bitmap.Config.ARGB_8888)
        val c = Canvas(bitmap)
        rootView.draw(c)

        return bitmap
    }

    private fun createViewChild(item: Item, context: Context) : View {
        val viewChild = LinearLayout(context)
        viewChild.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT, 10f)
        viewChild.orientation = LinearLayout.VERTICAL

        val grandChild1 = LinearLayout(context)
        grandChild1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
        grandChild1.orientation = LinearLayout.HORIZONTAL

        val name = TextView(context)
        name.text = item.name
        name.gravity = Gravity.START
        name.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT, 5f)
        name.textSize = 16f
        name.setTypeface(null, Typeface.BOLD)

        val quantity = TextView(context)
        quantity.text = item.quantity.toString()
        quantity.gravity = Gravity.START
        quantity.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT, 1.5f).apply { marginStart = 20}
        quantity.setTypeface(null, Typeface.ITALIC)

        val price = TextView(context)
        price.text = item.price.toString()
        price.gravity = Gravity.END
        price.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT, 3.5f).apply { marginStart = 20}

        grandChild1.addView(name)
        grandChild1.addView(quantity)
        grandChild1.addView(price)

        val itemTotalPrice = TextView(context)
        itemTotalPrice.text = (item.price * item.quantity).toString()
        itemTotalPrice.gravity = Gravity.END

        viewChild.addView(grandChild1)
        viewChild.addView(itemTotalPrice)
        return viewChild
    }*/
}
